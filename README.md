# About this repository

This repository is used to store the yolov5 models and make them available for use.

The code and artifacts used for the creation for the models are kept in the [hpc enabled yolov5 repository](https://git.scc.kit.edu/aiss_cv/hpc_enabled_frameworks/yolov5.git).

## Results of final dataset

| Dataset | Sub-Model | Weights    | Epochs | Precision | Recall | MAP50 | MAP50-95 |
| ------- | --------- | ---------- | ------ | --------- | ------ | ----- | -------- |
| v3_0    | n         | none       | 300    | 0.921     | 0.923  | 0.952 | 0.777    |
| v3_0    | n         | yolov5n.pt | 300    | 0.932     | 0.937  | 0.957 | 0.811    |
| v3_0    | s         | none       | 300    | 0.932     | 0.928  | 0.957 | 0.805    |
| v3_0    | s         | yolov5s.pt | 300    | 0.94      | 0.94   | 0.962 | 0.832    |
| v3_0    | m         | none       | 300    | 0.939     | 0.934  | 0.956 | 0.824    |
| v3_0    | m         | yolov5m.pt | 300    | 0.934     | 0.936  | 0.957 | 0.838    |
